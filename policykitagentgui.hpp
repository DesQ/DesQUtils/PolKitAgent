/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * This file was originally a part of LXQt.
    * LXQt - a lightweight, Qt based, desktop toolset
    * https://lxqt.org
    * Copyright: 2011-2012 Razor team
    * Authors:
    *   Petr Vanek <petr@scribus.info>
    * Original License: LGPL2 or any later version.
    *
    * Suitable modifications have been made for DesQ
    *

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#include <PolkitQt1/Details>
#include <PolkitQt1/Identity>

#include "ui_policykitagentgui.h"

class PolicykitAgentGUI : public QDialog, public Ui::PolicykitAgentGUI {
    Q_OBJECT;

    private:
        enum DataRoles {
            RolePromptText = Qt::UserRole,
            RolePromptEcho
        };

    public:
        PolicykitAgentGUI(const QString &actionId,
                          const QString &message,
                          const QString &iconName,
                          const PolkitQt1::Details &details,
                          const PolkitQt1::Identity::List &identities);

        void setPrompt(const PolkitQt1::Identity &identity, const QString &text, bool echo);

        /*! \brief Returns currently selected identity (serialized by toString())
         */
        QString identity();
        QString response();

    public slots:
        void onIdentityChanged(int index);

        void show();

};
