/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * This file was originally a part of LXQt.
    * LXQt - a lightweight, Qt based, desktop toolset
    * https://lxqt.org
    * Copyright: 2011-2012 Razor team
    * Authors:
    *   Petr Vanek <petr@scribus.info>
    * Original License: LGPL2 or any later version.
    *
    * Suitable modifications have been made for DesQ
    *

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QIcon>
#include "policykitagentgui.hpp"
#include <unistd.h>

#include <wlrootsqt/Application.hpp>
#include <wlrootsqt/Registry.hpp>
#include <wlrootsqt/InputInhibition.hpp>

PolicykitAgentGUI::PolicykitAgentGUI(const QString &actionId,
                                     const QString &message,
                                     const QString &iconName,
                                     const PolkitQt1::Details &details,
                                     const PolkitQt1::Identity::List &identities)
   : QDialog(0, Qt::WindowStaysOnTopHint) {
    setupUi(this);
    Q_UNUSED(actionId);
    Q_UNUSED(details); // it seems too confusing for end user (=me)

    setWindowIcon( QIcon::fromTheme( iconName, QIcon::fromTheme( "desq" ) ) );

    messageLabel->setText(message);
    QIcon icon = QIcon::fromTheme(iconName);
    if (icon.isNull())
        icon = QIcon::fromTheme(QLatin1String("dialog-question"));
    iconLabel->setPixmap(icon.pixmap(64, 64));

    const uid_t current_uid = getuid();
    int current_user_index = -1;
    for (const PolkitQt1::Identity& identity : identities) {

        const int i = identityComboBox->count(); // index of the added item
        identityComboBox->addItem(identity.toString());
        PolkitQt1::UnixUserIdentity const * const u_id = static_cast<const PolkitQt1::UnixUserIdentity *>(&identity);
        if (u_id != nullptr && u_id->uid() == current_uid)
            current_user_index = i;
    }

    if (current_user_index != -1)
        identityComboBox->setCurrentIndex(current_user_index);

    connect(
        identityComboBox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &PolicykitAgentGUI::onIdentityChanged
    );

    passwordEdit->setFocus(Qt::OtherFocusReason);
};

void PolicykitAgentGUI::setPrompt(const PolkitQt1::Identity &identity, const QString &text, bool echo) {

    const int ix = identityComboBox->findText(identity.toString());

    if (ix != -1) {
        identityComboBox->setItemData(ix, text, RolePromptText);
        identityComboBox->setItemData(ix, echo, RolePromptEcho);

        if (ix == identityComboBox->currentIndex()) {

            promptLabel->setText(text);
            passwordEdit->setEchoMode(echo ? QLineEdit::Normal : QLineEdit::Password);
        }
    }
};

QString PolicykitAgentGUI::identity() {

    Q_ASSERT(identityComboBox->currentIndex() != -1);
    return identityComboBox->currentText();
};

QString PolicykitAgentGUI::response() {

    QString response = passwordEdit->text();
    passwordEdit->setText(QString());
    return response;
};

void PolicykitAgentGUI::onIdentityChanged(int index) {

    QVariant text = identityComboBox->itemData(index, RolePromptText);
    QVariant echo = identityComboBox->itemData(index, RolePromptEcho);
    if (text != QVariant{})
        promptLabel->setText(text.toString());

    if (echo != QVariant{})
        passwordEdit->setEchoMode(echo.toBool() ? QLineEdit::Normal : QLineEdit::Password);
};

void PolicykitAgentGUI::show() {

    // DesQInputInhibitManager *inhibitMgr = qApp->waylandRegistry()->inputInhibitManager();
	// DesQInputInhibitor *inhibitor = inhibitMgr->getInputInhibitor();

    // qputenv( "QT_WAYLAND_SHELL_INTEGRATION", "DesQLock" );
    QDialog::exec();

    // DesQLayerSurface *cls = new DesQLayerSurface( windowHandle() );
    // cls->setup( qApp->waylandRegistry()->layerShell(), DesQLayerShell::Overlay, "de-widget" );
    // cls->setSurfaceSize( size() );
    // cls->setMargins( QMargins( 0, 0, 0, 0 ) );
    // cls->setKeyboardInteractivity( DesQLayerSurface::Exclusive );
    // cls->apply();

	// cls->setExclusiveZone( -1 );
	// cls->apply();

	// qunsetenv( "QT_WAYLAND_SHELL_INTEGRATION" );
};
