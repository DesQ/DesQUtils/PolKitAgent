/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * This file was originally a part of LXQt.
    * LXQt - a lightweight, Qt based, desktop toolset
    * https://lxqt.org
    * Copyright: 2011-2012 Razor team
    * Authors:
    *   Petr Vanek <petr@scribus.info>
    * Original License: LGPL2 or any later version.
    *
    * Suitable modifications have been made for DesQ
    *

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <glib-object.h>
#include <QCommandLineParser>

#include <wlrootsqt/Application.hpp>

#include "policykitagent.hpp"

int main( int argc, char *argv[] ) {

    // Retained from LXQtPolicykit
    #if !GLIB_CHECK_VERSION(2, 36, 0)
        g_type_init();
    #endif

    WlrootsQt::Application app( "DesQPolKitAgent", argc, argv );
    app.setQuitOnLastWindowClosed( false );

    QCommandLineParser parser;
    parser.setApplicationDescription( QStringLiteral( "DesQ Policykit Agent" ) );
    app.setApplicationVersion( "1.0.0" );
    parser.addVersionOption();
    parser.addHelpOption();
    parser.process( app );

    PolicykitAgent agent;

    return app.exec();
};
