# DesQ Polkit Agent
## PolicyKit Authenticator for DesQ DE

This is a authenticator for PolKit. Most of the code is taken from LXQt PolKit


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/PolKitAgent.git PolKitAgent`
- Enter the `PolKitAgent` folder
  * `cd PolKitAgent`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5 (qtbase5-dev, qtbase5-dev-tools)
* Qt
* libdesq (https://gitlab.com/DesQ/libdesq)
* wlrootsqt (https://gitlab.com/wlrootsqt/wlrootsqt)
* polkit-agent-1
* polkit-qt5-1


### Known Bugs
* Please test and let me know


### Upcoming
* Any other feature you request for... :)
