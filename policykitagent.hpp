/*
	*
	* Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * This file was originally a part of LXQt.
    * LXQt - a lightweight, Qt based, desktop toolset
    * https://lxqt.org
    * Copyright: 2011-2012 Razor team
    * Authors:
    *   Petr Vanek <petr@scribus.info>
    * Original License: LGPL2 or any later version.
    *
    * Suitable modifications have been made for DesQ
    *

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* at your option, any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#pragma once

#define POLKIT_AGENT_I_KNOW_API_IS_SUBJECT_TO_CHANGE 1

#include <PolkitQt1/Agent/Listener>
#include <PolkitQt1/Agent/Session>
#include <PolkitQt1/Details>
#include <PolkitQt1/Identity>

#include <QApplication>
#include <QHash>
#include <QMessageBox>

class PolicykitAgentGUI;

class PolicykitAgent : public PolkitQt1::Agent::Listener {
    Q_OBJECT;

    public:
        PolicykitAgent( QObject *parent = 0 );
        ~PolicykitAgent();

    public slots:
        void initiateAuthentication( const QString &actionId,
                                    const QString &message,
                                    const QString &iconName,
                                    const PolkitQt1::Details &details,
                                    const QString &cookie,
                                    const PolkitQt1::Identity::List &identities,
                                    PolkitQt1::Agent::AsyncResult *result );

        bool initiateAuthenticationFinish();
        void cancelAuthentication();

        void request( const QString &request, bool echo );
        void completed( bool gainedAuthorization );
        void showError( const QString &text );
        void showInfo( const QString &text );

    private:
        bool m_inProgress;
        PolicykitAgentGUI * m_gui;
        QMessageBox *m_infobox;
        QHash<PolkitQt1::Agent::Session*,PolkitQt1::Identity> m_SessionIdentity;
};
